import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  final String? text;
  final double height;
  final VoidCallback? onPressed;
  final bool isSecondary;

  const CustomButton(
      {Key? key,
      this.text,
      this.height = 40,
      this.onPressed,
      this.isSecondary = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final primaryColor = Theme.of(context).primaryColor;
    final color = isSecondary ? Colors.transparent : primaryColor;
    return InkWell(
      onTap: onPressed,
      child: Container(
        height: height,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(6),
          color: color,
        ),
        child: Center(
          child: Text(
            text ?? 'Button',
            overflow: TextOverflow.ellipsis,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 12,
              color: isSecondary
                  ? primaryColor
                  : color.computeLuminance() > 0.5
                      ? Colors.black
                      : Colors.white,
            ),
          ),
        ),
      ),
    );
  }
}
