import 'package:flutter/material.dart';
import 'package:gap/gap.dart';

class CustomTextField extends StatelessWidget {
  const CustomTextField({
    Key? key,
    this.label = '',
    this.hintText = '',
    this.obscureText = false,
    this.prefix,
    this.suffix,
    required this.controller,
    this.keyboardType = TextInputType.text,
  }) : super(key: key);
  final String label;
  final String hintText;
  final bool obscureText;
  final IconData? prefix;
  final IconData? suffix;
  final TextEditingController controller;
  final TextInputType keyboardType;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          label,
          style: TextStyle(fontSize: 12, fontWeight: FontWeight.w600),
        ),
        label == '' ? Container() : Gap(4),
        Container(
          height: 45,
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: Color(0xFF008FD7)),
            borderRadius: BorderRadius.circular(8),
          ),
          child: Center(
              child: Row(
            children: [
              Gap(16),
              prefix != null
                  ? Icon(
                      prefix,
                      color: Color(0xFF8C8CA1),
                    )
                  : Container(),
              prefix != null ? Gap(8) : Container(),
              Expanded(
                child: TextField(
                  keyboardType: keyboardType,
                  controller: controller,
                  decoration: InputDecoration(
                      isCollapsed: true,
                      border: InputBorder.none,
                      contentPadding: EdgeInsets.symmetric(vertical: 12),
                      hintText: hintText,
                      hintStyle:
                          TextStyle(color: Colors.black54, fontSize: 14)),
                  obscureText: obscureText,
                ),
              ),
              suffix != null ? Gap(8) : Container(),
              suffix != null
                  ? Icon(
                      suffix,
                      color: Color(0xFF8C8CA1),
                    )
                  : Container(),
              Gap(12),
            ],
          )),
        ),
      ],
    );
  }
}
