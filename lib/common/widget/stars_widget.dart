import 'package:flutter/material.dart';

class StarsWidget extends StatelessWidget {
  const StarsWidget(this.value, {Key? key, this.showValue = false})
      : super(key: key);
  final double value;
  final bool showValue;

  @override
  Widget build(BuildContext context) {
    double starWidth = ((value * 15) - 1.5) / 2;
    if (starWidth <= 0) {
      starWidth = 0;
    }
    return Stack(
      children: [
        Row(
          children: [
            showValue
                ? Text(
                    value.toString(),
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                    ),
                  )
                : Container(),
            showValue ? SizedBox(width: 10) : Container(),
            Container(
              height: 20,
              width: 100,
              child: ListView.builder(
                itemCount: 5,
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  return Container(
                    width: 15,
                    padding: EdgeInsets.only(right: 4),
                    child: Icon(
                      Icons.star,
                      size: 15,
                      color: Color(0xFFC4C4C4),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
        Row(
          children: [
            showValue
                ? Text(
                    value.toString(),
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                    ),
                  )
                : Container(),
            showValue ? SizedBox(width: 10) : Container(),
            Container(
              height: 20,
              width: starWidth,
              child: ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: 5,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  return Container(
                    width: 15,
                    padding: EdgeInsets.only(right: 4),
                    child: Icon(
                      Icons.star,
                      size: 15,
                      color: Color(0xFFFF6B00),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ],
    );
  }
}
