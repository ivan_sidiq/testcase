import 'package:flutter/material.dart';

class CustomErrorWidget extends StatelessWidget {
  const CustomErrorWidget({
    Key? key,
    this.padding = EdgeInsets.zero,
    this.borderRadius = BorderRadius.zero,
    this.message = 'Tap untuk refresh',
    required this.onTap,
  }) : super(key: key);

  final EdgeInsets padding;
  final Function onTap;
  final BorderRadius borderRadius;
  final String message;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: borderRadius,
      onTap: () => onTap(),
      child: Padding(
        padding: padding,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.error,
                size: 32,
                color: Color(0xFFDB5A5A),
              ),
              SizedBox(
                height: 8,
              ),
              Text(
                'Tap untuk refresh',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Color(0xFFDB5A5A),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
