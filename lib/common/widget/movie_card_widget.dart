import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:get_it/get_it.dart';
import 'package:majootestcase/common/widget/stars_widget.dart';
import 'package:majootestcase/models/movie.dart';
import 'package:majootestcase/services/navigation_service.dart';
import 'package:cached_network_image/cached_network_image.dart';

class MovieCardWidget extends StatelessWidget {
  const MovieCardWidget({Key? key, required this.movie, this.small = false})
      : super(key: key);
  final MovieBase movie;
  final bool small;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => GetIt.I<NavigationServiceMain>()
          .pushNamed('/movie_detail', args: movie),
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 14),
        width: small ? 200 : double.infinity,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          color: Color(0xFFFAFCFE),
          boxShadow: [
            BoxShadow(
              color: Color(0x1E000000),
              blurRadius: 5,
              spreadRadius: 0,
              offset: Offset(2, 2),
            )
          ],
        ),
        child: Column(
          children: [
            CachedNetworkImage(
                imageUrl: movie.image!.url!,
                fit: BoxFit.cover,
                placeholder: (context, url) => Container(
                      height: small ? 275 : 350,
                      width: small ? 200 : double.infinity,
                      decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.vertical(top: Radius.circular(12)),
                        color: Color(0x998C8CA1),
                      ),
                      child: Center(
                        child: Icon(
                          Icons.image,
                          color: Colors.white,
                        ),
                      ),
                    ),
                errorWidget: (context, url, error) => Container(
                      height: small ? 275 : 350,
                      width: small ? 200 : double.infinity,
                      decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.vertical(top: Radius.circular(12)),
                        color: Color(0x998C8CA1),
                      ),
                      child: Center(
                        child: Icon(
                          Icons.image,
                          color: Colors.white,
                        ),
                      ),
                    ),
                imageBuilder: (context, imgProvider) {
                  return Container(
                    height: small ? 275 : 350,
                    width: small ? 200 : double.infinity,
                    decoration: BoxDecoration(
                      borderRadius:
                          BorderRadius.vertical(top: Radius.circular(12)),
                      color: Color(0x998C8CA1),
                      image: DecorationImage(
                          image: imgProvider, fit: BoxFit.cover),
                    ),
                  );
                }),
            Container(
              padding: EdgeInsets.symmetric(vertical: 8, horizontal: 8),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    movie.title! + ' (' + movie.year!.toString() + ')',
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700),
                    overflow: TextOverflow.ellipsis,
                  ),
                  small ? Container() : Gap(8),
                  small
                      ? Container()
                      : StarsWidget(movie.rating!, showValue: true),
                  Gap(8),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
