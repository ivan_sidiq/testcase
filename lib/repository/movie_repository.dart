import 'package:majootestcase/models/movie.dart';
import 'package:majootestcase/utils/base_repository.dart';
import 'package:majootestcase/models/base_response.dart';
import 'package:majootestcase/models/movie_list.dart';

class MovieRepository extends BaseRepository {
  //Request get-top-rated-movies dari API imdb
  Future<BaseResponse> getTopMovies() async {
    final response = await fetch('title/get-top-rated-movies');

    if (response.statusCode == 200) {
      final List<MovieList> data =
          List.from(response.data).map((e) => MovieList.fromJson(e)).toList();

      return BaseResponse(
        statusCode: 200,
        data: data,
      );
    } else {
      return response;
    }
  }

  //Request get-base dari API imdb
  Future<BaseResponse> getMovieListDetail(String id) async {
    final response =
        await fetch('title/get-base', queryParameters: {'tconst': id});

    if (response.statusCode == 200) {
      final MovieBase data = MovieBase.fromJson(response.data);

      return BaseResponse(
        statusCode: 200,
        data: data,
      );
    } else {
      return response;
    }
  }

  //Request more-like-this dari API imdb
  Future<BaseResponse> getOtherMovieList(String id) async {
    print(id);
    final response = await fetch('title/get-more-like-this',
        queryParameters: {'tconst': id});

    if (response.statusCode == 200) {
      final List<String> data =
          List.from(response.data).map((e) => e as String).toList();
      return BaseResponse(
        statusCode: 200,
        data: data,
      );
    } else {
      return response;
    }
  }
}
