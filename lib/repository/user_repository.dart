import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:majootestcase/models/base_response.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class UserRepository extends BaseResponse {
  //Memeriksa apakah user sudah pernah login sebelumnya
  Future<BaseResponse> fetchLocalUser() async {
    final storage = FlutterSecureStorage();
    final name = await storage.read(key: kName);
    //final pass = await storage.read(key: kPassword);
    final email = await storage.read(key: kEmail);
    if (name == null) {
      return BaseResponse(
        statusCode: 404,
        message: 'not logged in',
      );
    } else {
      final data = User(userName: name, email: email!, password: '');
      return BaseResponse(
        statusCode: 200,
        data: data,
      );
    }
  }

  //Memeriksa data input oleh user dengan database lokal
  Future<BaseResponse> login(String email, String password) async {
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, 'testcase_majoo.db');

    Database database = await openDatabase(path, version: 1);
    final a =
        await database.rawQuery('SELECT * FROM User WHERE email = ?', [email]);
    if (a.isEmpty) {
      print('Anda belum memiliki akun di email ini');
      return BaseResponse(
        statusCode: 404,
        message: 'Anda belum memiliki akun di email ini',
      );
    } else {
      final user = User.fromJson(a.first);
      if (user.password != password) {
        print('Password anda salah');
        return BaseResponse(
          statusCode: 404,
          message: 'Password anda salah',
        );
      } else {
        final storage = FlutterSecureStorage();
        await storage.write(key: kName, value: user.userName);
        //await storage.write(key: kPassword, value: user.password);
        await storage.write(key: kEmail, value: user.email);
        return BaseResponse(
          statusCode: 200,
          data: user,
        );
      }
    }
  }

  //Simpan data user ke database lokal dan cache storage lokal
  Future<BaseResponse> register(
      String name, String email, String password) async {
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, 'testcase_majoo.db');

    Database database = await openDatabase(path, version: 1);
    final a =
        await database.rawQuery('SELECT * FROM User WHERE email = ?', [email]);
    if (a.isEmpty) {
      final b = await database.rawInsert(
          'INSERT INTO User(email, name, password) VALUES("$email", "$name", "$password")');
      final user = User(email: email, userName: name, password: password);
      final storage = FlutterSecureStorage();
      await storage.write(key: kName, value: user.userName);
      //await storage.write(key: kPassword, value: user.password);
      await storage.write(key: kEmail, value: user.email);
      return BaseResponse(
        statusCode: 200,
        data: user,
      );
    } else {
      return BaseResponse(
        statusCode: 404,
        message: 'Email sudah digunakan',
      );
    }
  }
}
