import 'package:bloc/bloc.dart';
import 'package:majootestcase/models/movie.dart';
import 'package:majootestcase/models/movie_list.dart';
import 'package:majootestcase/repository/movie_repository.dart';
import 'package:meta/meta.dart';

part 'movie_detail_state.dart';

class MovieDetailCubit extends Cubit<MovieDetailState> {
  MovieDetailCubit() : super(MovieDetailInitial());

  final _repo = MovieRepository();

  //Request rekomendasi movie berdasarkan movie yang dilihat
  Future<void> getOtherMovieList(String id) async {
    emit(OtherMovieLoading());

    final response =
        await _repo.getOtherMovieList(id.substring(7, id.length - 1));
    if (response.statusCode == 200) {
      List<String> mvl = response.data;
      List<MovieBase> mvb = [];
      final newList = mvl.take(5).toList();
      for (var i = 0; i < newList.length; i++) {
        mvb.add(
          await getMovieBaseList(
              newList[i].substring(7, newList[i].length - 1)),
        );
      }
      emit(OtherMovieSuccess(mvb));
    } else {
      emit(OtherMovieFailed());
    }
  }

  //Request detail movie berdasarkan id yg telah diproses di atas
  Future<MovieBase> getMovieBaseList(String id) async {
    final response = await _repo.getMovieListDetail(id);
    final a = response.data as MovieBase;

    return a;
  }
}
