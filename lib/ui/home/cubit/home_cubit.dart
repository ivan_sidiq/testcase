import 'package:bloc/bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/repository/user_repository.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:meta/meta.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

part 'home_state.dart';

class HomeCubit extends Cubit<HomeState> {
  HomeCubit() : super(HomeInitial());

  final _repo = UserRepository();

  //Membuat database SQLite lokal jika belum ada
  Future<void> fetchDatabase() async {
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, 'testcase_majoo.db');

    Database database = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      // When creating the db, create the table
      await db.execute(
          'CREATE TABLE User (id INTEGER PRIMARY KEY, email TEXT, name Text, password Text)');
      await db.execute(
          'INSERT INTO User(email, name, password) VALUES("dex@gmail.com", "Dex", "12345678")');
    });

    await fetchUser();

    //final a =
    //    await database.rawQuery('SELECT * FROM User WHERE name = ?', ['Dexy']);
    //print(a);
  }

  //Memeriksa apakah user sudah login sebelumnya
  Future<void> fetchUser() async {
    emit(HomeLoading());

    final response = await _repo.fetchLocalUser();
    if (response.statusCode == 200) {
      emit(HomeSuccess(response.data));
    } else {
      emit(HomeFailed());
    }
  }

  //Logout user
  Future<void> logout() async {
    final storage = FlutterSecureStorage();
    await storage.delete(key: kName);
    await storage.delete(key: kPassword);
    await storage.delete(key: kEmail);
  }
}
