part of 'movie_detail_cubit.dart';

@immutable
abstract class MovieDetailState {}

class MovieDetailInitial extends MovieDetailState {}

class OtherMovieLoading extends MovieDetailState {}

class OtherMovieSuccess extends MovieDetailState {
  final List<MovieBase> movieList;

  OtherMovieSuccess(this.movieList);
}

class OtherMovieFailed extends MovieDetailState {}
