part of 'movie_cubit.dart';

@immutable
abstract class MovieState {}

class MovieInitial extends MovieState {}

class MovieLoading extends MovieState {}

class MovieSuccess extends MovieState {
  final List<MovieBase> movieList;

  MovieSuccess(this.movieList);
}

class MovieFailed extends MovieState {}
