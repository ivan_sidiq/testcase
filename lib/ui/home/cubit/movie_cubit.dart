import 'package:bloc/bloc.dart';
import 'package:majootestcase/models/base_response.dart';
import 'package:majootestcase/models/movie.dart';
import 'package:majootestcase/models/movie_list.dart';
import 'package:majootestcase/repository/movie_repository.dart';
import 'package:meta/meta.dart';

part 'movie_state.dart';

class MovieCubit extends Cubit<MovieState> {
  MovieCubit() : super(MovieInitial());

  final _repo = MovieRepository();

  //Memproses id movie setelah mendapat data dari API
  Future<void> getMovieList() async {
    emit(MovieLoading());

    final response = await _repo.getTopMovies();
    if (response.statusCode == 200) {
      List<MovieList> mvl = response.data;
      List<MovieBase> mvb = [];
      final newList = mvl.take(10).toList();
      for (var i = 0; i < newList.length; i++) {
        mvb.add(
          await getMovieBaseList(newList[i].chartRating!,
              newList[i].id!.substring(7, newList[i].id!.length - 1)),
        );
      }
      emit(MovieSuccess(mvb));
    } else {
      emit(MovieFailed());
    }
  }

  //Request data detail movie setelah id terproses
  Future<MovieBase> getMovieBaseList(double rating, String id) async {
    final response = await _repo.getMovieListDetail(id);
    final a = response.data as MovieBase;
    final b = MovieBase(
      rating: rating,
      type: a.type,
      id: a.id,
      image: a.image,
      title: a.title,
      titleType: a.titleType,
      year: a.year,
    );
    return b;
  }
}
