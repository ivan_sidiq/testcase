part of 'home_cubit.dart';

@immutable
abstract class HomeState {}

class HomeInitial extends HomeState {}

class HomeLoading extends HomeState {}

class HomeSuccess extends HomeState {
  final User user;

  HomeSuccess(this.user);
}

class HomeFailed extends HomeState {}
