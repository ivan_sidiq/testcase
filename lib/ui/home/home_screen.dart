import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:gap/gap.dart';
import 'package:get_it/get_it.dart';
import 'package:majootestcase/common/widget/custom_error_widget.dart';
import 'package:majootestcase/common/widget/custom_loading_widget.dart';
import 'package:majootestcase/common/widget/movie_card_widget.dart';
import 'package:majootestcase/services/navigation_service.dart';
import 'package:majootestcase/ui/home/cubit/home_cubit.dart';
import 'package:majootestcase/ui/home/cubit/movie_cubit.dart';
import 'package:majootestcase/utils/toast.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => HomeCubit(),
        ),
        BlocProvider(
          create: (context) => MovieCubit(),
        ),
      ],
      child: _HomeScreen(),
    );
  }
}

class _HomeScreen extends HookWidget {
  const _HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _cubit = context.read<HomeCubit>();
    final _mCubit = context.read<MovieCubit>();

    useEffect(() {
      _cubit.fetchDatabase();
      return;
    }, [_cubit]);

    return Scaffold(
      appBar: AppBar(
        actions: [
          InkWell(
            onTap: () {
              _cubit.logout();
              GetIt.I<NavigationServiceMain>().pushReplacementNamed('/login');
              showToastSuccess('Berhasil Logout');
            },
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Center(
                child: Text(
                  'logout',
                  style: TextStyle(color: Colors.blue),
                ),
              ),
            ),
          )
        ],
        backgroundColor: Colors.white,
        titleTextStyle: TextStyle(
            color: Colors.black, fontSize: 16, fontWeight: FontWeight.bold),
        title: BlocBuilder<HomeCubit, HomeState>(
          builder: (context, state) {
            if (state is HomeSuccess) {
              return Text('Hallo ' + (state.user.userName ?? ''));
            }
            return Text('Imdb TestCase');
          },
        ),
      ),
      body: SafeArea(
        child: BlocConsumer<HomeCubit, HomeState>(
          listener: (context, state) {
            if (state is HomeFailed) {
              GetIt.I<NavigationServiceMain>().pushReplacementNamed('/login');
            } else if (state is HomeSuccess) {
              _mCubit.getMovieList();
            }
          },
          builder: (context, state) {
            if (state is HomeSuccess) {
              final userName = state.user.userName!;
              return BlocConsumer<MovieCubit, MovieState>(
                listener: (context, state) {
                  if (state is MovieSuccess) {
                    print(state.movieList);
                  } else if (state is MovieFailed) {
                    print('failed');
                  } else if (state is MovieLoading) {
                    print('is loading');
                  }
                },
                builder: (context, state) {
                  if (state is MovieSuccess) {
                    return Container(
                      child: Center(
                        child: SingleChildScrollView(
                          physics: BouncingScrollPhysics(),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Gap(16),
                              ListView.builder(
                                  physics: NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: state.movieList.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Container(
                                      margin: EdgeInsets.only(bottom: 36),
                                      child: InkWell(
                                          onTap: () {},
                                          child: MovieCardWidget(
                                              movie: state.movieList[index])),
                                    );
                                  }),
                            ],
                          ),
                        ),
                      ),
                    );
                  } else if (state is MovieFailed) {
                    return Container(
                      child: Center(child: CustomErrorWidget(
                        onTap: () {
                          _cubit.fetchDatabase();
                        },
                      )),
                    );
                  } else if (state is MovieLoading) {
                    return Container(
                      child: Center(child: CustomLoadingWidget()),
                    );
                  } else {
                    return Container(
                      child: Center(child: CustomErrorWidget(
                        onTap: () {
                          _cubit.fetchDatabase();
                        },
                      )),
                    );
                  }
                },
              );
            }
            return Container(
              child: Center(child: CustomLoadingWidget()),
            );
          },
        ),
      ),
    );
  }
}
