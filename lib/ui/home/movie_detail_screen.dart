import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:gap/gap.dart';
import 'package:majootestcase/common/widget/custom_error_widget.dart';
import 'package:majootestcase/common/widget/custom_loading_widget.dart';
import 'package:majootestcase/common/widget/movie_card_widget.dart';
import 'package:majootestcase/common/widget/stars_widget.dart';
import 'package:majootestcase/models/movie.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:majootestcase/ui/home/cubit/movie_detail_cubit.dart';

class MovieDetailScreen extends StatelessWidget {
  const MovieDetailScreen({Key? key, required this.movieBase})
      : super(key: key);
  final MovieBase movieBase;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => MovieDetailCubit(),
      child: _MovieDetailScreen(movieBase: movieBase),
    );
  }
}

class _MovieDetailScreen extends HookWidget {
  const _MovieDetailScreen({Key? key, required this.movieBase})
      : super(key: key);
  final MovieBase movieBase;

  @override
  Widget build(BuildContext context) {
    final _cubit = context.read<MovieDetailCubit>();

    useEffect(() {
      _cubit.getOtherMovieList(movieBase.id!);

      return;
    }, [_cubit]);
    return Scaffold(
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CachedNetworkImage(
                imageUrl: movieBase.image!.url!,
                fit: BoxFit.cover,
                placeholder: (context, url) => Container(
                      height: 450,
                      width: double.infinity,
                      child: Center(
                        child: Icon(
                          Icons.image,
                          color: Colors.white,
                        ),
                      ),
                    ),
                errorWidget: (context, url, error) => Container(
                      height: 450,
                      width: double.infinity,
                      child: Center(
                        child: Icon(
                          Icons.image,
                          color: Colors.white,
                        ),
                      ),
                    ),
                imageBuilder: (context, imgProvider) {
                  return Container(
                    height: 450,
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color: Color(0x998C8CA1),
                      image: DecorationImage(
                          image: imgProvider, fit: BoxFit.cover),
                    ),
                  );
                }),
            Gap(4),
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Text(
                movieBase.title! + ' (' + movieBase.year!.toString() + ')',
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700),
              ),
            ),
            Gap(16),
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Text(
                'Serial Lainnya',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700),
              ),
            ),
            BlocBuilder<MovieDetailCubit, MovieDetailState>(
              builder: (context, state) {
                if (state is OtherMovieSuccess) {
                  return Container(
                    height: 365,
                    child: ListView.builder(
                        physics: BouncingScrollPhysics(),
                        scrollDirection: Axis.horizontal,
                        itemCount: state.movieList.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            margin: EdgeInsets.only(bottom: 36),
                            child: InkWell(
                                onTap: () {},
                                child: MovieCardWidget(
                                  movie: state.movieList[index],
                                  small: true,
                                )),
                          );
                        }),
                  );
                } else if (state is OtherMovieFailed) {
                  return Container(
                    child: Center(child: CustomErrorWidget(
                      onTap: () {
                        _cubit.getOtherMovieList(movieBase.id!);
                      },
                    )),
                  );
                } else if (state is OtherMovieLoading) {
                  return Container(
                    child: Center(child: CustomLoadingWidget()),
                  );
                } else {
                  return Container(
                    child: Center(child: CustomErrorWidget(
                      onTap: () {
                        _cubit.getOtherMovieList(movieBase.id!);
                      },
                    )),
                  );
                }
              },
            )
          ],
        ),
      ),
    );
  }
}
