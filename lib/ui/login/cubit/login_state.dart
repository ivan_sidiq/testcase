part of 'login_cubit.dart';

@immutable
abstract class LoginState {}

class LoginInitial extends LoginState {}

class LoginSuccess extends LoginState {
  final User user;

  LoginSuccess(this.user);
}

class LoginLoading extends LoginState {}

class LoginFailed extends LoginState {
  final String message;

  LoginFailed(this.message);
}
