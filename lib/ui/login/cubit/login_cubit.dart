import 'package:bloc/bloc.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/repository/user_repository.dart';
import 'package:majootestcase/utils/toast.dart';
import 'package:meta/meta.dart';

part 'login_state.dart';

class LoginCubit extends Cubit<LoginState> {
  LoginCubit() : super(LoginInitial());

  final _repo = UserRepository();

  void validate(String email, String password) {
    final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
    if (email == '' || password == '') {
      emit(LoginFailed(
          'Form tidak boleh kosong, mohon periksa kembali data yang anda masukkan.'));
    } else if (pattern.hasMatch(email)) {
      _login(email, password);
    } else {
      emit(LoginFailed('Mohon masukkan email yang valid'));
    }
  }

  Future<void> _login(String email, String password) async {
    emit(LoginLoading());

    final response = await _repo.login(email, password);
    if (response.statusCode == 200) {
      showToastSuccess('Login Berhasil');
      emit(LoginSuccess(response.data));
    } else {
      emit(LoginFailed(response.message ?? ''));
    }
  }
}
