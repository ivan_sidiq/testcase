part of 'register_cubit.dart';

@immutable
abstract class RegisterState {}

class RegisterInitial extends RegisterState {}

class RegisterSuccess extends RegisterState {
  final User user;

  RegisterSuccess(this.user);
}

class RegisterLoading extends RegisterState {}

class RegisterFailed extends RegisterState {
  final String message;

  RegisterFailed(this.message);
}
