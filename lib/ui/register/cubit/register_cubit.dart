import 'package:bloc/bloc.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/repository/user_repository.dart';
import 'package:majootestcase/utils/toast.dart';
import 'package:meta/meta.dart';

part 'register_state.dart';

class RegisterCubit extends Cubit<RegisterState> {
  RegisterCubit() : super(RegisterInitial());

  final _repo = UserRepository();

  //validasi input oleh user
  void validate(String email, String name, String password) {
    final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
    if (email == '' && password == '' && name == '') {
      emit(RegisterFailed(
          'Form tidak boleh kosong, mohon periksa kembali data yang anda masukkan.'));
    } else if (pattern.hasMatch(email)) {
      _register(name, email, password);
    } else {
      emit(RegisterFailed('Mohon masukkan email yang valid'));
    }
  }

  //registrasi user
  Future<void> _register(String name, String email, String password) async {
    emit(RegisterLoading());

    final response = await _repo.register(name, email, password);
    if (response.statusCode == 200) {
      showToastSuccess('Register Berhasil');
      emit(RegisterSuccess(response.data));
    } else {
      emit(RegisterFailed(response.message ?? ''));
    }
  }
}
