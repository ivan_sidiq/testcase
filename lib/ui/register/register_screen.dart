import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gap/gap.dart';
import 'package:get_it/get_it.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/services/navigation_service.dart';
import 'package:majootestcase/utils/toast.dart';

import 'cubit/register_cubit.dart';

class RegisterScreen extends StatelessWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => RegisterCubit(),
      child: _RegisterScreen(),
    );
  }
}

class _RegisterScreen extends StatelessWidget {
  const _RegisterScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _cubit = context.read<RegisterCubit>();
    final _emailController = TextEditingController();
    final _nameController = TextEditingController();
    final _passwordController = TextEditingController();

    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Gap(48),
                Text(
                  'Selamat Datang',
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    // color: colorBlue,
                  ),
                ),
                Text(
                  'Silahkan login terlebih dahulu',
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                Gap(24),
                _Form(
                  nameC: _nameController,
                  emailC: _emailController,
                  passwordC: _passwordController,
                ),
                Gap(24),
                BlocListener<RegisterCubit, RegisterState>(
                  listener: (context, state) {
                    if (state is RegisterSuccess) {
                      GetIt.I<NavigationServiceMain>()
                          .pushReplacementNamed('/home');
                    } else if (state is RegisterFailed) {
                      showToastError(state.message);
                    }
                  },
                  child: CustomButton(
                    text: 'Register',
                    onPressed: () {
                      _cubit.validate(_emailController.text,
                          _nameController.text, _passwordController.text);
                    },
                  ),
                ),
                Gap(24),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Sudah punya akun? ',
                      style: TextStyle(color: Colors.black45),
                    ),
                    Gap(8),
                    InkWell(
                      onTap: () {
                        GetIt.I<NavigationServiceMain>()
                            .pushReplacementNamed('/login');
                      },
                      child: Text(
                        'Login',
                        style: TextStyle(color: Colors.blue),
                      ),
                    ),
                  ],
                ),
                Gap(48),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _Form extends StatelessWidget {
  const _Form(
      {Key? key,
      required this.emailC,
      required this.passwordC,
      required this.nameC})
      : super(key: key);
  final TextEditingController emailC;
  final TextEditingController passwordC;
  final TextEditingController nameC;

  @override
  Widget build(BuildContext context) {
    GlobalKey<FormState> formKey = new GlobalKey<FormState>();
    return Container(
      child: Form(
        key: formKey,
        child: Column(
          children: [
            CustomTextField(
              label: 'Nama',
              keyboardType: TextInputType.text,
              controller: nameC,
              hintText: 'Nama',
            ),
            Gap(10),
            CustomTextField(
              label: 'Email',
              keyboardType: TextInputType.emailAddress,
              controller: emailC,
              hintText: 'example@123.com',
            ),
            Gap(10),
            CustomTextField(
              label: 'Password',
              keyboardType: TextInputType.visiblePassword,
              obscureText: true,
              controller: passwordC,
              hintText: 'Password',
            ),
          ],
        ),
      ),
    );
  }
}
