import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:majootestcase/models/movie.dart';
import 'package:majootestcase/ui/home/home_screen.dart';
import 'package:majootestcase/ui/home/movie_detail_screen.dart';
import 'package:majootestcase/ui/login/login_page.dart';
import 'package:majootestcase/ui/register/register_screen.dart';

void configureRoutes() {
  final router = GetIt.I<FluroRouter>();

  router.notFoundHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
      return _RoutesNotFound();
    },
  );

  router.define(
    '/home',
    handler: Handler(
      handlerFunc: (BuildContext? context, Map<String, List<String>> params) =>
          HomeScreen(),
    ),
    transitionType: TransitionType.none,
  );

  router.define(
    '/login',
    handler: Handler(
      handlerFunc: (BuildContext? context, Map<String, List<String>> params) =>
          LoginPage(),
    ),
    transitionType: TransitionType.none,
  );

  router.define(
    '/register',
    handler: Handler(
      handlerFunc: (BuildContext? context, Map<String, List<String>> params) =>
          RegisterScreen(),
    ),
    transitionType: TransitionType.none,
  );

  router.define(
    '/movie_detail',
    handler: Handler(
        handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
      final args = context!.arguments as MovieBase;
      return MovieDetailScreen(movieBase: args);
    }),
    transitionType: TransitionType.none,
  );
}

class _RoutesNotFound extends StatelessWidget {
  const _RoutesNotFound({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Routes not found'),
          ],
        ),
      ),
    );
  }
}
