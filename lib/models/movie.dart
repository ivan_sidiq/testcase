class MovieBase {
  double? rating = -1;
  String? type;
  String? id;

  Image? image;
  String? title;
  String? titleType;
  int? year;

  MovieBase(
      {this.rating = -1,
      this.type,
      this.id,
      this.image,
      this.title,
      this.titleType,
      this.year});

  MovieBase.fromJson(Map<String, dynamic> json) {
    type = json['@type'];
    id = json['id'];
    image = json['image'] != null ? new Image.fromJson(json['image']) : null;
    title = json['title'];
    titleType = json['titleType'];
    year = json['year'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['@type'] = this.type;
    data['id'] = this.id;
    if (this.image != null) {
      data['image'] = this.image!.toJson();
    }
    data['title'] = this.title;
    data['titleType'] = this.titleType;
    data['year'] = this.year;
    return data;
  }
}

class Image {
  int? height;
  String? id;
  String? url;
  int? width;

  Image({this.height, this.id, this.url, this.width});

  Image.fromJson(Map<String, dynamic> json) {
    height = json['height'];
    id = json['id'];
    url = json['url'];
    width = json['width'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['height'] = this.height;
    data['id'] = this.id;
    data['url'] = this.url;
    data['width'] = this.width;
    return data;
  }
}
