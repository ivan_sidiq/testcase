class MovieList {
  String? id;
  double? chartRating;

  MovieList({this.id, this.chartRating});

  MovieList.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    chartRating = json['chartRating'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['chartRating'] = this.chartRating;
    return data;
  }
}
