import 'dart:developer';

import 'package:easy_localization/easy_localization.dart';
import 'package:fluro/fluro.dart';
import 'package:flutter/services.dart';
import 'package:majootestcase/services/di_service.dart';
import 'package:majootestcase/services/navigation_service.dart';
import 'package:majootestcase/services/routes.dart';
import 'package:majootestcase/ui/home/home_screen.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
import 'package:get_it/get_it.dart';
import 'package:flutter/material.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Color(0x19000000),
    statusBarIconBrightness: Brightness.dark,
  ));

  FlutterError.onError = (details) {
    log(details.exceptionAsString(), stackTrace: details.stack);
  };
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
    DIService.initialize();
    configureRoutes();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'TestCase Majoo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      navigatorObservers: [
        SentryNavigatorObserver(),
      ],
      navigatorKey: GetIt.I<NavigationServiceMain>().navigatorKey,
      onGenerateRoute: GetIt.I<FluroRouter>().generator,
      home: const HomeScreen(),
    );
  }
}
