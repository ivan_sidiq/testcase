import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    hide Options;
import 'package:get_it/get_it.dart';
import 'package:retry/retry.dart';

import '../models/base_response.dart';
import '../models/meta.dart';
import 'constant.dart';
import 'error_helper.dart';

class BaseRepository {
  final Dio dio = Dio();
  final FlutterSecureStorage secureStorage = GetIt.I<FlutterSecureStorage>();
  final String baseApi = kBaseApi;

  Future<BaseResponse> fetch(String api,
      {Map<String, dynamic>? queryParameters}) async {
    try {
      //final token = await secureStorage.read(key: kToken);

      Map<String, dynamic> headers = {};
      //if (token != null) headers['Authorization'] = 'Bearer $token';
      headers = {
        'X-RapidAPI-Host': 'imdb8.p.rapidapi.com',
        'X-RapidAPI-Key': '08e59ceae7msh72316f0fed66f91p1f5392jsn91a5c99f32f9'
      };

      final response = await retry(
        () => dio.get(
          baseApi + api,
          queryParameters: queryParameters,
          options: Options(responseType: ResponseType.json, headers: headers),
        ),
        retryIf: (e) => e is SocketException || e is TimeoutException,
      );

      return BaseResponse(
        statusCode: response.statusCode,
        data: response.data,
      );
    } on DioError catch (e) {
      return ExceptionHelper(e).catchException();
    }
  }

  Future<BaseResponse> post(
    String api, {
    Map<String, dynamic>? data,
  }) async {
    try {
      final token = await secureStorage.read(key: kToken);

      final Map<String, dynamic> headers = {};
      if (token != null) headers['Authorization'] = 'Bearer $token';

      final response = await retry(
        () => dio.post(
          api,
          data: json.encode(data),
          options: Options(responseType: ResponseType.json, headers: headers),
        ),
        retryIf: (e) => e is SocketException || e is TimeoutException,
      );

      return BaseResponse(
        statusCode: response.statusCode,
        data: response.data['data'],
        message: response.data['message'],
      );
    } on DioError catch (e) {
      return ExceptionHelper(e).catchException();
    }
  }

  Future<BaseResponse> put(String api,
      {Map<String, dynamic>? data,
      Map<String, dynamic>? queryParameters}) async {
    try {
      final token = await secureStorage.read(key: kToken);

      final Map<String, dynamic> headers = {};
      if (token != null) headers['Authorization'] = 'Bearer $token';

      final response = await retry(
        () => dio.put(
          api,
          data: json.encode(data),
          queryParameters: queryParameters,
          options: Options(headers: headers),
        ),
        retryIf: (e) => e is SocketException || e is TimeoutException,
      );

      return BaseResponse(
        statusCode: response.statusCode,
        data: response.data['data'],
        message: response.data['message'],
      );
    } on DioError catch (e) {
      return ExceptionHelper(e).catchException();
    }
  }

  Future<BaseResponse> delete(String api,
      {Map<String, dynamic>? data,
      Map<String, dynamic>? queryParameters}) async {
    try {
      final token = await secureStorage.read(key: kToken);

      final Map<String, dynamic> headers = {};
      if (token != null) headers['Authorization'] = 'Bearer $token';

      final response = await retry(
        () => dio.delete(
          api,
          data: json.encode(data),
          queryParameters: queryParameters,
          options: Options(headers: headers),
        ),
        retryIf: (e) => e is SocketException || e is TimeoutException,
      );

      return BaseResponse(
        statusCode: response.statusCode,
        data: response.data['data'],
        message: response.data['message'],
      );
    } on DioError catch (e) {
      return ExceptionHelper(e).catchException();
    }
  }

  Future<BaseResponse> postFormData(
    String api, {
    FormData? data,
  }) async {
    try {
      final token = await secureStorage.read(key: kToken);

      final Map<String, dynamic> headers = {};
      if (token != null) headers['Authorization'] = 'Bearer $token';

      final response = await retry(
        () => dio.post(
          api,
          data: data,
          options: Options(responseType: ResponseType.json, headers: headers),
        ),
        retryIf: (e) => e is SocketException || e is TimeoutException,
      );

      return BaseResponse(
        statusCode: response.statusCode,
        data: response.data['data'],
        message: response.data['message'],
      );
    } on DioError catch (e) {
      return ExceptionHelper(e).catchException();
    }
  }

  Future<BaseResponse> loginRegister(
    String api, {
    Map<String, dynamic>? data,
    Map<String, dynamic>? queryParameters,
  }) async {
    try {
      final response = await retry(
        () => dio.post(
          api,
          data: json.encode(data),
          queryParameters: queryParameters,
          options: Options(responseType: ResponseType.json),
        ),
        retryIf: (e) => e is SocketException || e is TimeoutException,
      );

      if (response.data['token'] != null) {
        GetIt.I<FlutterSecureStorage>()
            .write(key: kToken, value: response.data['token']);
      }

      return BaseResponse(
        statusCode: response.statusCode,
        data: response.data['data'],
        message: response.data['message'],
      );
    } on DioError catch (e) {
      return ExceptionHelper(e).catchException();
    }
  }
}
