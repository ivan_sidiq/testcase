//================Key=============================
const String kBaseApi = 'https://imdb8.p.rapidapi.com/';
const String kToken = 'token';
const String kName = 'name';
const String kEmail = 'email';
const String kPassword = 'password';

//=================Error Messages==================
const String kErrorCantReachServer = "Can't reach to the http server";
const String kErrorException = "Terjadi kesalahan. Coba lagi";
const String kConnectionTimeout = "Connection Timeout";
const String kNoInternetConnection = "No Internet Connection";
const String kLoginUsernamePasswordWrongMessage =
    "Email atau password salah. Coba kembali";
